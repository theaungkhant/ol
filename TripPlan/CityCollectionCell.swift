//
//  CityCollectionCell.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/25/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//

import UIKit

class CityCollectionCell: UICollectionViewCell {
    
    @IBOutlet var cityName: UILabel!
    @IBOutlet var cityImg: UIImageView!
    
    
}
