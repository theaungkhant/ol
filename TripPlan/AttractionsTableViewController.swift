//
//  AttractionsTableViewController.swift
//  TripPlan
//
//  Created by Aung Khant Thet Naing on 1/26/16.
//  Copyright © 2016 Aung Khant Thet Naing. All rights reserved.
//
import MapKit
import UIKit

class AttractionsTableViewController: PFQueryTableViewController, MKMapViewDelegate {
    private var attractions = [Attraction]()
    @IBOutlet var mapView:MKMapView!
   
  
    
    
    
    // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadTripsFromParse()
        tableView.dataSource = self
        tableView.delegate = self
        //   super.view.addSubview(tb)
        tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.reloadData()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadList:",name:"load", object: nil)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
    }
    
    
    func loadList(notification: NSNotification){
        //load data here
        self.tableView.reloadData()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        // Configure the PFQueryTableView
        self.parseClassName = "attractions"
        self.textKey = "name"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
    }
    
    
    override func queryForTable() -> PFQuery{
        let query = PFQuery(className: "attractions")
        query.orderByAscending("name")
        return query
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return attractions.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AttractionsTableViewCell
        
        
        let trip = attractions[indexPath.row]
        
        cell.nameLabel.text = trip.name
        cell.typeLabel.text = trip.type
        cell.attractionImg.image = UIImage()
        if let featuredImage = trip.featuredImage {
            featuredImage.getDataInBackgroundWithBlock({ (imageData: NSData?, error: NSError?) -> Void in
                if let tripImageData = imageData {
                    cell.attractionImg.image = UIImage(data:tripImageData)
                }
            })
        }
        
        
        //   let img = UIImage(named: "question")
        //  cell.attractionImg.image = img
        
        
        
        return cell
    }
    
    
    
    
    @IBAction func close(segue:UIStoryboardSegue) {
    }
    
    
    
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showAttractionDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destinationViewController as! AttractionsDetailViewController
                destinationController.attractions = attractions[indexPath.row]
            }
        }
    }
    
    func loadTripsFromParse() {
        // Clear up the array
        attractions.removeAll(keepCapacity: true)
        tableView.reloadData()
        
        // Pull data from Parse
        let query = PFQuery(className:"attractions")
        query.cachePolicy = PFCachePolicy.NetworkElseCache
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            
            if let error = error {
                print("Error: \(error) \(error.userInfo)")
                return
            }
            
            if let objects = objects {
                for (index, object) in objects.enumerate() {
                    // Convert PFObject into Trip object
                    let trip = Attraction(pfObject: object)
                    self.attractions.append(trip)
                    
                    let indexPath = NSIndexPath(forRow: index, inSection: 0)
                    //    self.tableView.indexPaths([indexPath])
                    
                    //   self.tableView?.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
                    
                    self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
                    self.tableView.endUpdates()
                    
                    
                    
                }
            }
            
        }
    }
    
       
    
    
    
    
    
    
}